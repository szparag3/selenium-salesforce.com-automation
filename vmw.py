#! python3
import pyperclip, time, re, datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

print("""Standalone functions:
eaTransfer() - transfer details and update in LATF section (eaFrom,eaTo,SR,<'u'>)
cso() - CSO survey and case comment (csoSR)
caseComment() - case comments in parent case (SR, TEXT)
sfdcActivity()- create activity for call/cso/legal(srNum, option="call", Comment, reciver, phoneNum)""")




#componnt
def loginToSfdc(driver):
    #login to SFDC requre browser driver
    driver.implicitly_wait(30)
    driver.get('https://login.salesforce.com/')
    driver.find_element_by_id('username').send_keys(sfdcUser)
    driver.find_element_by_id('password').send_keys(sfdcPasswd)
    driver.find_element_by_id('password').submit()

#component
def loginToAP(driver):
    #login to Admin portal requre browser driver    
    driver.implicitly_wait(30)
    #login
    driver.get('https://www.vmware.com/admin/')
    driver.find_element_by_id('username').send_keys(adUser)
    driver.find_element_by_id('password').send_keys(adPasswd)
    driver.find_element_by_id('password').submit()
    driver.find_element_by_id('username').send_keys(adUser)
    driver.find_element_by_id('password').send_keys(adPasswd)
    driver.find_element_by_id('password').submit()

#component
def closeTheBrowser(driver):
    driver.quit()







#component
def eaDetails(driver, eaNum):
    eaNum = str(eaNum)

    try:
        eaDetails = {}
        eaDetails['eaNumber'] = eaNum
        #colecting from information
        driver.find_element_by_link_text('User Profile Search').click()
        driver.find_element_by_link_text('EA Search').click()
        driver.find_element_by_id('number').send_keys(eaNum)
        driver.find_element_by_id('search').click()
        driver.find_element_by_link_text(eaNum).click()
        driver.find_element_by_id('usr').click()
        #find ea name
        eaName = driver.find_element_by_xpath("//div[@id='eaHeaderDetailsLeft']/div[contains(.,'EA Name:')]/b")
        #find ea details
        eaDetail = driver.find_elements_by_xpath("//li[@id='eaDetailsContent']/table/tbody/tr/td")

        #check if eaDetail/eaName var are present:
        
        ##check eaName
        while eaName.text == '':
            time.sleep(1)
            print('waiting for eaName')
        eaDetails['eaName'] = eaName.text

        ##check eaDetail section
        for i in range(1,6):
            while eaDetail[i].text == '':
                time.sleep(1)
                print('Ea %s waiting for eaDetails %s' %(eaNum, ['Segment', 'Type', 'Source', 'Preferred Renewals Partner', 'Status'][i]))
        eaDetails['eaSegment'] = eaDetail[1].text
        eaDetails['eaType'] = eaDetail[2].text
        eaDetails['eaStatus'] = eaDetail[5].text

        if eaDetails['eaStatus'] == 'ACTIVE':

            #find SU details
            superUser = driver.find_elements_by_xpath("//table[@id='usrTbl']//tr[contains(.,'Super User')]/td")

            
            #find PC details
            procurementContact = driver.find_elements_by_xpath("//table[@id='usrTbl']//tr[contains(.,'Procurement Contact')]/td")


            #check if su/pc var are present


            ##check SU details 
            for i in range(1,5):
                while superUser[i].text == '':
                    time.sleep(1)
                    print('Ea %s waiting for superUser %s' %(eaNum, ['Number', 'First Name', 'Last Name', 'Email'][i-1]))
            eaDetails['suNumber'] = superUser[1].text
            eaDetails['suFirstName'] = superUser[2].text
            eaDetails['suLastName'] = superUser[3].text
            eaDetails['suEmail'] = superUser[4].text

            ##check PC details 
            for i in range(1,5):
                while procurementContact[i].text == '':
                    time.sleep(1)
                    print('waiting for procurementContact'+str(i)+'is empty')
            eaDetails['pcNumber'] = procurementContact[1].text
            eaDetails['pcFirstName'] = procurementContact[2].text
            eaDetails['pcLastName'] = procurementContact[3].text
            eaDetails['pcEmail'] = procurementContact[4].text
     
        #print output
        return eaDetails
    except:
        driver.refresh()      

        error_message = str(eaNum)
        
        try:
            alert = driver.switch_to_alert()
            alert.dismiss()
        except:
            pass

#standalone function
def eaTransfer(inputFrom=0,inputTo=0, srNum='', option=''):
    
    #OPTION LIST: "u" update with out prompt
    option = str(option)
    option = option.lower().strip()

    #get input from user
    if inputFrom == 0:
        inputFrom = input('What are from EAs (separated by ";"):')
    if inputTo == 0:
        inputTo = input('What are To EAs (separated by ";"):')
    inputFrom = str(inputFrom)
    inputTo = str(inputTo)

#TODO user data validation
    
    #change input to list
    inputFrom = inputFrom.split(';')
    inputTo = inputTo.split(';')
    
    for i in range(len(inputFrom)):
        inputFrom[i] = inputFrom[i].strip()
    for i in range(len(inputTo)):
        inputTo[i] = inputTo[i].strip()

    #open the browser
    browserAP = webdriver.Firefox()
    #login to Admin Potal 
    loginToAP(browserAP)


    #reg ex pattern
    eaRegex = re.compile(r'\d{9}')
    
    print('reading From EAs...')
    for i in range(len(inputFrom)):
        if eaRegex.search(inputFrom[i]) != None:
            inputFrom[i] = eaDetails(browserAP, inputFrom[i])
            if inputFrom[i]['eaStatus'] == 'ACTIVE':
                #get phone number 
                browserAP.find_element_by_link_text('Update Profile').click()
                browserAP.find_element_by_name('emailAddress').send_keys(inputFrom[i]['suNumber'])
                browserAP.find_element_by_css_selector('input[type="submit"][value="Update Customer Profile"]').click()
                inputFrom[i]['suPhone'] = browserAP.find_element_by_name('customer.primaryPhoneNumber').get_attribute("value")
                if inputFrom[i]['suPhone'] != '':
                    inputFrom[i]['suPhone'] = 'ph ' + inputFrom[i]['suPhone']
                
                


                if inputFrom[i]['suNumber'] == inputFrom[i]['pcNumber']:
                    inputFrom[i]['pcPhone'] = inputFrom[i]['suPhone']
                else:
                    browserAP.find_element_by_link_text('Update Profile').click()
                    browserAP.find_element_by_name('emailAddress').send_keys(inputFrom[i]['pcNumber'])
                    browserAP.find_element_by_css_selector('input[type="submit"][value="Update Customer Profile"]').click()
                    inputFrom[i]['pcPhone'] = browserAP.find_element_by_name('customer.primaryPhoneNumber').get_attribute("value")     
                    if inputFrom[i]['pcPhone'] != '':
                        inputFrom[i]['pcPhone'] = 'ph ' + inputFrom[i]['pcPhone']

    print('reading To EAs...')
    for i in range(len(inputTo)):
        if eaRegex.search(inputTo[i]) != None:
            inputTo[i] = eaDetails(browserAP, inputTo[i])

            #get phone number 
            browserAP.find_element_by_link_text('Update Profile').click()
            browserAP.find_element_by_name('emailAddress').send_keys(inputTo[i]['suNumber'])
            browserAP.find_element_by_css_selector('input[type="submit"][value="Update Customer Profile"]').click()
            inputTo[i]['suPhone'] = browserAP.find_element_by_name('customer.primaryPhoneNumber').get_attribute("value")
            if inputTo[i]['suPhone'] == '':
                inputTo[i]['suPhone'] = 'ph <phone number not found>'
            else:
                inputTo[i]['suPhone'] = 'ph ' + inputTo[i]['suPhone']
            if inputTo[i]['suNumber'] == inputTo[i]['pcNumber']:
                inputTo[i]['pcPhone'] = inputTo[i]['suPhone']
            else:
                browserAP.find_element_by_link_text('Update Profile').click()
                browserAP.find_element_by_name('emailAddress').send_keys(inputTo[i]['pcNumber'])
                browserAP.find_element_by_css_selector('input[type="submit"][value="Update Customer Profile"]').click()
                inputTo[i]['pcPhone'] = browserAP.find_element_by_name('customer.primaryPhoneNumber').get_attribute("value")
                if inputTo[i]['pcPhone'] == '':
                    inputTo[i]['pcPhone'] = 'ph <phone number not found>'
                else:
                    inputTo[i]['pcPhone'] = 'ph ' + inputTo[i]['pcPhone']

            



    #Design output
    print()
    for i in range(len(inputFrom)):
        if i == 0:
            print('===FROM:===')
        else:
            print()
            print('AND')
        if type(inputFrom[i]) == str:
            print('New Account')
            print('EA Name: ' + inputFrom[i])
        else:
            print('{} - {} account'.format(inputFrom[i]['eaSegment'],inputFrom[i]['eaType']))
            print('EA Number: ' + inputFrom[i]['eaNumber'])
            print('EA Name: ' + inputFrom[i]['eaName'])
            if inputFrom[i]['eaStatus'] == 'ACTIVE':
                if inputFrom[i]['suNumber'] == inputFrom[i]['pcNumber']:
                    print('%s %s - %s - %s - Super User, Procurement Contact' % (inputFrom[i]['suFirstName'], inputFrom[i]['suLastName'], inputFrom[i]['suEmail'], inputFrom[i]['suPhone']))
                else:
                    print('%s %s - %s - %s - Super User' % (inputFrom[i]['suFirstName'], inputFrom[i]['suLastName'], inputFrom[i]['suEmail'], inputFrom[i]['suPhone']))
                    print('%s %s - %s - %s - Procurement Contact' % (inputFrom[i]['pcFirstName'], inputFrom[i]['pcLastName'], inputFrom[i]['pcEmail'], inputFrom[i]['pcPhone']))
            else:
                print('account status - ' + inputFrom[i]['eaStatus'])
    print()
        
    for i in range(len(inputTo)):
        if i == 0:
            print('===TO:===')
        else:
            print()
            print('AND')

        if type(inputTo[i]) == str:
            print('New Account')
            print('EA Name: ' + inputTo[i])
        else:
            print('{} - {} account'.format(inputTo[i]['eaSegment'],inputTo[i]['eaType']))
            print('EA Number: ' + inputTo[i]['eaNumber'])
            print('EA Name: ' + inputTo[i]['eaName'])
            if inputTo[i]['eaStatus'] == 'ACTIVE':
                if inputTo[i]['suNumber'] == inputTo[i]['pcNumber']:
                    print('%s %s - %s - %s - Super User, Procurement Contact' % (inputTo[i]['suFirstName'], inputTo[i]['suLastName'], inputTo[i]['suEmail'], inputTo[i]['suPhone']))
                else:
                    print('%s %s - %s - %s - Super User' % (inputTo[i]['suFirstName'], inputTo[i]['suLastName'], inputTo[i]['suEmail'], inputTo[i]['suPhone']))
                    print('%s %s - %s - %s - Procurement Contact' % (inputTo[i]['pcFirstName'], inputTo[i]['pcLastName'], inputTo[i]['pcEmail'], inputTo[i]['pcPhone']))
            else:
                print('account status - ' + inputTo[i]['eaStatus'])
    print()
    
    #close AP
    closeTheBrowser(browserAP)
    
    if srNum == '':
        srNum = input('SR number for update (or empty for close):')
    srNum = str(srNum).strip()

        
    if srNum.isdigit():
        print('Login to SFDC ...')
        browserSFDC = webdriver.Firefox()
        loginToSfdc(browserSFDC)
        openSR(browserSFDC, srNum)
        if option != 'u':
            print('Assignor')
            assignorLegalName = input('Assignor Legal name:')
            assignorCountry = input('Assignor Country:')
            print('Assignee')
            assigneeLegalName = input('Assignee Legal name:')
            assigneeCountry = input('Assignee Country:')
            print('Transfer')
            transferType = input('(F)ull or (P)artial:').strip().lower()
            if transferType == 'p':
                transferPartial = input('contract split 0/1:') or '0'
            transferEla = input('ELA number [def=0]:') or '0'
            salesRep = input('Sales Rep:')
            
            print("""
******
EA (S)plit
EA (M)erge
(A)CF
(L)ATF (S)tandard
(L)ATF (N)ON-STANDARD""")
            reasonVS = input('Transfer reason:')
            reasonVS = reasonVS.lower().strip()
            if reasonVS == 's':
                print("""
***EA Split***
1 - EA Split - Booking Error
2 - EA Split - Merge Error Post conversion
3 - EA Split - OEM data issue
4 - EA Split - Person party (DR)
5 - EA Split - Uni/Dept
6 - EA Split - VSUS/Temp account""")
            elif reasonVS == 'm':
                print("""
***EA Merge***
1 - EA Merge - Customer Business requirement
2 - EA Merge - Duplicate due to DR
3 - EA Merge - Duplicate due to OEM
4 - EA Merge - Duplicate EA created at booking
5 - EA Merge - Duplicate EA created at booking (ELA)""")
            elif reasonVS == 'a':
                print("""
ACF
1 - ACF - Name Change
2 - ACF - Order Delivered Incorrectly
3 - ACF - Order Delivered Incorrectly (OEM)""")
            elif reasonVS == 'ls':
                print("""
***LATF Standard***
1 - LATF - Acquisition
2 - LATF - Merger
3 - LATF - Bankruptcy
4 - LATF - Contractually Agreed
5 - LATF - Divestiture >80%
6 - LATF - Gov Machinery
7 - LATF - Outsourcing
8 - LATF - Reversed Outsourcing - Prev. Agreement
9 - LATF - Subsidiary - same Territory""")
            elif reasonVS == 'ln':
                print("""
***LATF NON-STANDARD***
1 - LATF - Non Standard - Rev Outsourcing
2 - LATF - Non Standard - Applicable Region Laws
3 - LATF - Non Standard - Subsidiary
4 - LATF - Non Standard - Divestiture
5 - LATF - Non Standard - Across Geo Transfer
6 - LATF - Non Standard - Legal agreement involved
7 - LATF - Non Standard - License Compliance recommendation
8 - LATF - Non Standard - Previous LATF correction
9 - LATF - Non Standard - Sale of Licenses
0 - LATF - Non Standard - Other""")
            elif reasonVS == '':
                pass
            else:
                print("option not avaliabe")
                reasonVS = ''
            if reasonVS != '':   
                reasonVS2 = input('Transfer sub-reason:')
            print("""
******
1 - Identification - Gathering Transfer Details
2 - Identification - Legal Consultation requested
3 - Validation - Suppporting Documentation Requested
4 - Approval - Submitted to Legal
5 - Execution – Submitted to Legal Admin
6 - Execution - Processing LATF
7 - Execution - Change Confirmation""")
            workingStatus = input('Working Status:')
            

        browserSFDC.find_element_by_name('edit').click()
        if type(inputFrom[0]) == str:
            print('EA-from - details not avaliabe')
        else:
            browserSFDC.find_element_by_id('00N800000052QUV').clear()
            browserSFDC.find_element_by_id('00N800000052QUV').send_keys(inputFrom[0]['eaNumber'])
            browserSFDC.find_element_by_id('00N800000052QUa').clear()
            browserSFDC.find_element_by_id('00N800000052QUa').send_keys(inputFrom[0]['eaName'])
        if type(inputTo[0]) == str:
            browserSFDC.find_element_by_id('00N800000052RhA').clear()
            browserSFDC.find_element_by_id('00N800000052RhA').send_keys(inputTo[0])
        else:
            browserSFDC.find_element_by_id('00N800000052QVE').clear()
            browserSFDC.find_element_by_id('00N800000052QVE').send_keys(inputTo[0]['eaNumber'])
            browserSFDC.find_element_by_id('00N800000052RhA').clear()
            browserSFDC.find_element_by_id('00N800000052RhA').send_keys(inputTo[0]['eaName'])
        if option != 'u': 
            if assignorLegalName != '':
                browserSFDC.find_element_by_id('00N800000052QUf').clear()
                browserSFDC.find_element_by_id('00N800000052QUf').send_keys(assignorLegalName)
                browserSFDC.find_element_by_id('00N800000052QUk').clear()
                browserSFDC.find_element_by_id('00N800000052QUk').send_keys(assignorLegalName)
            if assignorCountry != '':
                browserSFDC.find_element_by_id('00N800000052QUp').send_keys(assignorCountry)
                
            if assigneeLegalName != '':
                browserSFDC.find_element_by_id('00N800000052RhF').clear()
                browserSFDC.find_element_by_id('00N800000052RhF').send_keys(assigneeLegalName)
                browserSFDC.find_element_by_id('00N800000052RhK').clear()
                browserSFDC.find_element_by_id('00N800000052RhK').send_keys(assigneeLegalName)
            if assigneeCountry != '':
                browserSFDC.find_element_by_id('00N800000052RhP').send_keys(assigneeCountry)
            if transferType != '':
                if transferType == 'f':
                    browserSFDC.find_element_by_id('00N80000005TWR4').send_keys('Full')
                elif transferType == 'p':
                    browserSFDC.find_element_by_id('00N80000005TWR4').send_keys('Partial')
                    if transferPartial == '1':
                        if browserSFDC.find_element_by_id('00N800000052QV4').is_selected():
                            pass
                        else:
                            browserSFDC.find_element_by_id('00N800000052QV4').click()
                    if transferPartial == '0':
                        if browserSFDC.find_element_by_id('00N800000052QV4').is_selected():
                            browserSFDC.find_element_by_id('00N800000052QV4').click()
                            
                else:
                    print('===Wrong transfer type')
        
            if transferEla != '0':
                browserSFDC.find_element_by_id('00N800000052QUu').clear()
                browserSFDC.find_element_by_id('00N800000052QUu').send_keys(transferEla)
            if salesRep != '':
                browserSFDC.find_element_by_id('00N800000052RhU').clear()
                browserSFDC.find_element_by_id('00N800000052RhU').send_keys(salesRep)
            if reasonVS != '':
                if reasonVS == 's':
                    browserSFDC.find_element_by_id('00N80000004o145').send_keys('EA Split')
                    if reasonVS2 != '':
                        if reasonVS2 == '1':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – Booking Error')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – Booking Error')
                        elif reasonVS2 == '2':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – Merge Error post conversion')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – Merge Error post conversion')
                        elif reasonVS2 == '3':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – OEM data issue')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – OEM data issue')
                        elif reasonVS2 == '4':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – Person party (DR)')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – Person party (DR)')
                        elif reasonVS2 == '5':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – Uni/Dept')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – Uni/Dept')
                        elif reasonVS2 == '6':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – VSUS/Temp account')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Split – VSUS/Temp account')
                        else:
                            print('===wrong sub-reason')
                elif reasonVS == 'm':
                    browserSFDC.find_element_by_id('00N80000004o145').send_keys('EA Merge')
                    if reasonVS2 != '':
                        if reasonVS2 == '1':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge - Customer Business requirement')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge - Customer Business requirement')
                        elif reasonVS2 == '2':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge - Duplicate due to DR')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge - Duplicate due to DR')
                        elif reasonVS2 == '3':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge - Duplicate due to OEM')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge - Duplicate due to OEM')
                        elif reasonVS2 == '4':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge – Duplicate EA created at booking')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge – Duplicate EA created at booking')
                        elif reasonVS2 == '5':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge - Duplicate EA created at booking (ELA)')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('EA Merge - Duplicate EA created at booking (ELA)')
                        else:
                            print('===wrong sub-reason')
                elif reasonVS == 'a':
                    browserSFDC.find_element_by_id('00N80000004o145').send_keys('Account Maintenance')
                    if reasonVS2 != '':
                        if reasonVS2 == '1':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('ACF – Name Change')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('ACF – Name Change')
                        elif reasonVS2 == '2':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('ACF – Order Delivered Incorrectly')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('ACF – Order Delivered Incorrectly')
                        elif reasonVS2 == '3':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('ACF – Order Delivered Incorrectly (OEM)')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('ACF – Order Delivered Incorrectly (OEM)')
                        else:
                            print('===wrong sub-reason')
                elif reasonVS == 'ls':
                    browserSFDC.find_element_by_id('00N80000004o145').send_keys('License Transfer - Standard')
                    if reasonVS2 != '':
                        if reasonVS2 == '1':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Acquisition')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Acquisition')
                        elif reasonVS2 == '2':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Merger')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Merger')
                        elif reasonVS2 == '3':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Bankruptcy')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Bankruptcy')
                        elif reasonVS2 == '4':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Contractually Agreed')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Contractually Agreed')
                        elif reasonVS2 == '5':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Divestiture > 80%')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Divestiture > 80%')
                        elif reasonVS2 == '6':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Gov Machinery')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Gov Machinery')
                        elif reasonVS2 == '7':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Outsourcing')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Outsourcing')
                        elif reasonVS2 == '8':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Reversed Outsourcing - Prev. Agreement')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Reversed Outsourcing - Prev. Agreement')
                        elif reasonVS2 == '9':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Subsidiary - same Territory')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Subsidiary - same Territory')
                        else:
                            print('===wrong sub-reason')
                elif reasonVS == 'ln':
                    browserSFDC.find_element_by_id('00N80000004o145').send_keys('License Transfer - Non-Standard')
                    if reasonVS2 != '':
                        if reasonVS2 == '1':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Rev Outsourcing')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Rev Outsourcing')
                        elif reasonVS2 == '2':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Applicable Region Laws')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Applicable Region Laws')
                        elif reasonVS2 == '3':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Subsidiary')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Subsidiary')
                        elif reasonVS2 == '4':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Divestiture')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Divestiture')
                        elif reasonVS2 == '5':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Across Geo Transfer')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Across Geo Transfer')
                        elif reasonVS2 == '6':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Legal agreement involved')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Legal agreement involved')
                        elif reasonVS2 == '7':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - License Compliance recommendation')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - License Compliance recommendation')
                        elif reasonVS2 == '8':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Previous LATF correction')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Previous LATF correction')
                        elif reasonVS2 == '9':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Sale of Licenses')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard - Sale of Licenses')
                        elif reasonVS2 == '0':
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard – Other')
                            browserSFDC.find_element_by_id('00N80000004o144').send_keys('LATF - Non Standard – Other')
                        else:
                            print('===wrong sub-reason')
                else:
                    print('===wrong transfer reason')
            if workingStatus != '':
                if workingStatus == '1':
                    browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Identification - Gathering Transfer Details')
                elif workingStatus == '2':
                    browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Identification - Legal Consultation requested')
                elif workingStatus == '3':
                    browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Validation - Suppporting Documentation Requested')
                elif workingStatus == '4':
                    browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Approval - Submitted to Legal')
                elif workingStatus == '5':
                    browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Execution – Submitted to Legal Admin')
                elif workingStatus == '6':
                    browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Execution - Processing LATF')
                elif workingStatus == '7':
                    browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Execution - Change Confirmation')
                else:
                    print('===wrong working status!!!')


        
        browserSFDC.find_element_by_name('save').click()
        
        closeTheBrowser(browserSFDC)
        print('===LATF details updated')
    





    
###regex for SR
##
##    srRegex = re.compile(r'\d{11}')
##    if sr.Regex.search(sr) != None:
##        #open the browser
##        sfdcBrowser = webdriver.Firefox()
##        #login to sfdc
##        loginToSfdc(sfdcBrowser)
##        sfdcCade(sfdcBrowser, sr)
##        #check who is the case owner
##        proceed = input('SR%s - Owner %s ' % (sr, ))
##        #close
##        closeTheBrowser(sfdcBrowser)
##        
##
##    #OUTPUT
##    output = {}
##    output['FROM'] = eaDetailsTo
##    output['TO'] = eaDetailsFrom
          #return is currently not needed  
##    return output


##def sfdcCade(driver, sr):
##    sr = str(sr)
##    driver.find_element_by_id('phSearchInput').send_keys(sr)
##    driver.find_element_by_id('phSearchInput').submit()
##    browser.find_element_by_link_text(sr).click()


#component
def openSR(driver, srNum):
    srNum = str(srNum).strip()
    driver.find_element_by_link_text('Cases').click()
    driver.find_element_by_id('phSearchInput').send_keys(srNum)
    driver.find_element_by_id('phSearchInput').submit()
    driver.find_element_by_link_text(srNum).click()

    
#standalone function   
def cso(csoSR = 0):
    csoSR = str(csoSR)
    if csoSR == '0':
        csoSR = input('CSO case number for survey:')
        csoSR = csoSR.strip()
    #TODO check if survey was not sent before

    #login to SFDC
    browserSFDC = webdriver.Firefox()
    loginToSfdc(browserSFDC)
    openSR(browserSFDC, csoSR)

    #check if this is CSO case
    if browserSFDC.find_element_by_id('RecordType_ileinner').text == 'CSO [Change]':
        browserSurvey = webdriver.Firefox()
        browserSurvey.implicitly_wait(30)
        browserSurvey.get('https://docs.google.com/forms/d/1sXkXq6uNpZ248gg87WAClNe-urZlEVoogr2i9pBe-xA/viewform?edit_requested=true')
        browserSurvey.find_element_by_id('entry_1666906631').send_keys(csoSR)
        browserSurvey.find_element_by_id('group_1459451649_10').click()
        browserSurvey.find_element_by_id('group_175713081_10').click()
        browserSurvey.find_element_by_id('group_140989350_10').click()
        browserSurvey.find_element_by_id('group_1487969477_10').click()
        browserSurvey.find_element_by_id('group_1692167203_10').click()
        browserSurvey.find_element_by_id('ss-submit').click()
        if browserSurvey.find_element_by_class_name('ss-resp-message').text == "Thank you! We'll take your feedback and continue to improve our services":
            closeTheBrowser(browserSurvey)
            print('===survey submited for CSO case ' + csoSR)
            parentCase = browserSFDC.find_element_by_id('cas28_ileinner').text
            browserSFDC.find_element_by_link_text(parentCase).click()
            if browserSFDC.find_element_by_id('00N80000004o144_ileinner').text != ' ' and browserSFDC.find_element_by_id('00N800000052ae9_ileinner').text == ' ':
                resolutionOption = 'update'
            else:
                resolutionOption = ''
            workStatusForUpdate = ['Execution - Processing LATF', 'Execution – Submitted to Legal Admin', 'Approval - Submitted to Legal']
            if browserSFDC.find_element_by_id('00N800000052Rhe_ileinner').text in workStatusForUpdate:
                browserSFDC.find_element_by_name('edit').click()
                if resolutionOption == 'update':
                    browserSFDC.find_element_by_id('00N800000052ae9').send_keys('LATF - Transfer Completed')
                browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Execution - Change Confirmation')
                browserSFDC.find_element_by_name('save').click()
            if browserSFDC.find_element_by_id('cas3_ileinner').text == sfdcOwner:
                parentCase = browserSFDC.find_element_by_id('cas28_ileinner').text
                browserSFDC.find_element_by_link_text(parentCase).click()
                if browserSFDC.find_element_by_id('00N800000052Rhe_ileinner').text == 'Execution - Processing LATF':
                    browserSFDC.find_element_by_name('edit').click()
                    browserSFDC.find_element_by_id('00N800000052Rhe').send_keys('Execution - Change Confirmation')
                    browserSFDC.find_element_by_name('save').click()
            browserSFDC.find_element_by_name('newComment').click()
            browserSFDC.find_element_by_id('CommentBody').send_keys('CSO ' + csoSR +' processed' + prefix)
            browserSFDC.find_element_by_name('save').click()
            if browserSFDC.find_element_by_id('cas3_ileinner').text != '':
                closeTheBrowser(browserSFDC)
                print('===case comments submited in CSAS-LATF case '+ parentCase)

    else:
        print('This is not CSO case number')
        closeTheBrowser(browserSFDC)
    

#component
def CommentComponent(driver, srNum=0, comments_text='work in progress'):
    if srNum == 0:
        srNum = input('SR number for comments (separated by ","):')
        comments_text = input('provide case comments:')


    srNum = srNum.split(',')
    for i in range(len(srNum)):
        srNum[i] = srNum[i].strip()
##        openSR(driver, srNum[i])
        if driver.find_element_by_id('cas3_ileinner').text == sfdcOwner:
            childCaseSR = 'related to child case ' + srNum[i] + ' - '
        else:
            childCaseSR = ''
        while True:
            if driver.find_element_by_id('cas3_ileinner').text == sfdcOwner:
                parentCase = driver.find_element_by_id('cas28_ileinner').text
                driver.find_element_by_link_text(parentCase).click()
            else:
                break
        driver.find_element_by_name('newComment').click()
        driver.find_element_by_id('CommentBody').send_keys(childCaseSR + comments_text + prefix)
        driver.find_element_by_name('save').click()
        if driver.find_element_by_id('cas3_ileinner').text != '':
            print('===case comments submited for CSAS-LATF case ' + srNum[i])


#standalone function    
def caseComment(srNum=0, comments_text='work in progress'):
    if srNum == 0:
        srNum = input('SR number for comments (separated by ","):')
        
        comments_text = input('provide case comments:')
    srNum = str(srNum)
    browserSFDC = webdriver.Firefox()
    loginToSfdc(browserSFDC) 
    srNum = srNum.split(',')
    for i in range(len(srNum)):
        srNum[i] = srNum[i].strip()
        openSR(browserSFDC, srNum[i])
        if browserSFDC.find_element_by_id('cas3_ileinner').text == sfdcOwner:
            childCaseSR = 'related to child case ' + srNum[i] + ' - '
        else:
            childCaseSR = ''
        while True:
            if browserSFDC.find_element_by_id('cas3_ileinner').text == sfdcOwner:
                parentCase = browserSFDC.find_element_by_id('cas28_ileinner').text
                browserSFDC.find_element_by_link_text(parentCase).click()
            else:
                break
        browserSFDC.find_element_by_name('newComment').click()
        browserSFDC.find_element_by_id('CommentBody').send_keys(childCaseSR + comments_text + '\n' + prefix)
        browserSFDC.find_element_by_name('save').click()
        if browserSFDC.find_element_by_id('cas3_ileinner').text != '':
            print('===case comments submited for CSAS-LATF case ' + srNum[i])


    closeTheBrowser(browserSFDC)

#component
def submitActivity(driver, srNum, activitySubject, activityType, activityComments='', taskSubject='', dateChange=2):
    driver.find_element_by_name('new').click()
    #delay beacuse of chatter loading - check if chatter is present
    driver.find_element_by_id('buddy_list_min_text')
    driver.find_element_by_xpath("//input[@value='Continue']").click()
    driver.find_element_by_id('tsk5').clear()
    driver.find_element_by_id('tsk5').send_keys(activitySubject)
    driver.find_element_by_id('tsk10').send_keys(activityType)
    driver.find_element_by_id('tsk6').send_keys(activityComments)
    driver.find_element_by_id('tsk5_fu').send_keys(taskSubject)
    if taskSubject != '':
        taskDate = datetime.datetime.now() + datetime.timedelta(days=dateChange)
        #if new date is SUN or SAT add two more days /bussines days
        if taskDate.strftime('%w') == '0' or taskDate.strftime('%w') == '6':
            taskDate = taskDate + datetime.timedelta(days=2)
        taskDate = taskDate.strftime('%d/%m/%Y')
        driver.find_element_by_id('tsk4_fu').send_keys(taskDate)

    driver.find_element_by_name('save').click()
    

#standalone function
def sfdcActivity(srNum=0, option='', comment='', reciver='', phoneNum=''):
    #input if function has no arguments
    if srNum == 0:
        srNum = input('SR number related to call (separate by ","):')
    srNum = str(srNum).strip()
    #option list 
    optionList = {'call': 'to log a call',
                  'c': 'to set remainder for CSO ticket',
                  'l': 'to set remainder for Legal ticket',
                  'o': 'to set remainder and activity for other waiting for'}
    if option == '':

        print()
        for opt in optionList:
            print('%s - %s' % (opt, optionList[opt]))
        option = input('choose option [def=call]:') or 'call'
    option = option.strip().lower()
    if option in optionList:
        print('SFDC login...')
        browserSFDC = webdriver.Firefox()
        loginToSfdc(browserSFDC)
        openSR(browserSFDC, srNum)

        if option == 'call':
            caseRequestorSFDC = browserSFDC.find_element_by_id('cas3_ileinner').text
            caseRequestorPhoneSFDC = browserSFDC.find_element_by_id('cas9_ileinner').text
            caseRequestorEmailSFDC = browserSFDC.find_element_by_id('cas10_ileinner').text
            if caseRequestorEmailSFDC[-11:].lower() == '@vmware.com':
                caseRequestorSFDC = caseRequestorSFDC + ' - VMW'
            caseRequestor = input('Provide reciver name [def=%s]' % (caseRequestorSFDC)) or caseRequestorSFDC
            if caseRequestor == caseRequestorSFDC:
                caseRequestorPhone = input('Provide phone number [def=%s]' % (caseRequestorPhoneSFDC)) or caseRequestorPhoneSFDC
            else:
                caseRequestorPhone = input('Provide phone number:')
            print("""x - no Call - outside of business hours
0 - Call NO success
1 - Call success - commit
2 - Call success - case review'
3 - Call inboud
w - webex
i - No Call - internal case all documents in case""")

            comment = input('provide comment or choose shortcut ^^^:')
            comment = comment.strip()
            if comment == 'x':
                CommentComponent(browserSFDC, srNum, comments_text='Not able to call - outside of business hours')
            elif comment == '0':
                activitySubjectVar = 'Call Outbound - %s - NO Success' % (caseRequestor)
                activityCommentsVar = 'Call Outbound - NO Success\n%s\n%s\n%s' % (caseRequestor, caseRequestorPhone, prefix)
                submitActivity(browserSFDC, srNum, activitySubjectVar, 'Call Manual', activityCommentsVar)
            elif comment == '1':
                activitySubjectVar = 'Call Outbound - %s - Case Commit' % (caseRequestor)
                activityCommentsVar = 'Call Outbound - Case Commit\n%s\n%s\n%s' % (caseRequestor, caseRequestorPhone, prefix)
                submitActivity(browserSFDC, srNum, activitySubjectVar, 'Call Manual', activityCommentsVar)
            elif comment == '2':
                activitySubjectVar = 'Call Outbound - %s - Case Review' % (caseRequestor)
                activityCommentsVar = 'Call Outbound - Case Review\n%s\n%s\n%s' % (caseRequestor, caseRequestorPhone, prefix)
                submitActivity(browserSFDC, srNum, activitySubjectVar, 'Call Manual', activityCommentsVar)
            elif comment == '3':
                activitySubjectVar = 'Call Inbound Answered - %s' % (caseRequestor)
                activityCommentsVar = 'Call Inbound Answered\n%s\n%s\n%s' % (caseRequestor, caseRequestorPhone, prefix)
                submitActivity(browserSFDC, srNum, activitySubjectVar, 'Call Manual', activityCommentsVar)
            elif comment == 'w':
                activitySubjectVar = 'WebEx - %s - Case Review' % (caseRequestor)
                activityCommentsVar = '%s\nWebEx - Case Review\n%s\n%s\n%s' % (caseRequestor, caseRequestorPhone, prefix)
                submitActivity(browserSFDC, srNum, activitySubjectVar, 'Call Manual', activityCommentsVar)
            elif comment == 'i':
                CommentComponent(browserSFDC, srNum, comments_text='No commit Call - Internal VMware case - all documents arleady in case')
            else:
                activitySubjectVar = 'Call Outbound - %s' % (caseRequestor)
                activityCommentsVar = 'Call Outbound\n%s\n%s\n\n%s\n%s' % (caseRequestor, caseRequestorPhone, comment, prefix)
                submitActivity(browserSFDC, srNum, activitySubjectVar, 'Call Manual', activityCommentsVar)
            print('===call loged in SFDC')
        elif option == 'o':
            if comment == '':
                comment = input('provide subject for task and activity:')
            if comment != '':
                comment = '#' + comment
                submitActivity(browserSFDC, srNum, comment, '', '', comment, 2)
        else:
            print('option not avaliabe')
            

            
        closeTheBrowser(browserSFDC)
    else:
        print('option %s is not avaliabe yet' % (option))