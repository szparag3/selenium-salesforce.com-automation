from selenium import webdriver
import time ,pprint



report = {}

if eaList == '':
    eaList = input('provide EA Number (separated by ";"):')
#split ea number to list
eaList = eaList.split(';')
print('Step 1/4 - AP - {} EAs'.format(len(eaList)))

#login to AP to get party numbers
apBrowser = webdriver.Firefox()
apBrowser.implicitly_wait(30)
apBrowser.get('https://www.vmware.com/admin/')
apBrowser.find_element_by_id('username').send_keys(adUser)
apBrowser.find_element_by_id('password').send_keys(adPasswd)
apBrowser.find_element_by_id('password').submit()
apBrowser.find_element_by_id('username').send_keys(adUser)
apBrowser.find_element_by_id('password').send_keys(adPasswd)
apBrowser.find_element_by_id('password').submit()

#check party numbers in AP

i_ea = 0
qty_party = 0
for eaNum in eaList:
    eaNum = eaNum.strip()
    
    i_ea += 1
    print('AP - {} - {}%'.format(eaNum,round((i_ea-1)*100/len(eaList))))
    
    report[eaNum] = {}
    apBrowser.find_element_by_link_text('EA Search').click()
    apBrowser.find_element_by_id('number').send_keys(eaNum)
    apBrowser.find_element_by_id('search').click()
    apBrowser.find_element_by_link_text(eaNum).click()
    apBrowser.find_element_by_id('actCrsReferences').click()
    seleniumParty = apBrowser.find_elements_by_css_selector('li[id="actCrsReferencesContent"] tr')
    eaName = apBrowser.find_element_by_css_selector('div[id="eaHeaderDetailsLeft"] div:last-of-type b')
    eaDetails = apBrowser.find_elements_by_css_selector('li[id="eaDetailsContent"] tbody tr td')
    report[eaNum]['party'] = {}
    for i in (range(1,len(seleniumParty))):
        while seleniumParty[i].text == '':
            time.sleep(1)
        
        partyNum = seleniumParty[i].text
        partyNum = partyNum[:-16]
        qty_party += 1
        report[eaNum]['party'][partyNum] = {}
    report[eaNum]['eaName'] = eaName.text
    report[eaNum]['eaSegment'] = eaDetails[1].text
    report[eaNum]['eaType'] = eaDetails[2].text
    report[eaNum]['eaStatus'] = eaDetails[5].text
print('AP - 100%')
apBrowser.quit()

print('Step 2/4 - ORA - {} Parties'.format(qty_party))
#login do oracle to get instance details
oracleBrowser = webdriver.Firefox()
oracleBrowser.implicitly_wait(10)
oracleBrowser.get('http://oracleapps.vmware.com/OA_HTML/fndvald.jsp?username={}&password={}&langCode=US&requestUrl=APPSHOMEPAGE'.format(oracleUser, oraclePass))
oracleBrowser.find_element_by_link_text('VM Installed Base Inquiry').click()
oracleBrowser.find_element_by_link_text('Oracle Installed Base').click()
i_party = 0
qty_ib = 0
for eaNum in report:
    for party in report[eaNum]['party']:
        i_party += 1
        print('ORA - {} - {} - {}%'.format(eaNum,party,round((i_party-1)*100/qty_party)) )
        
        oracleBrowser.find_element_by_link_text('Advanced Search').click()
        oracleBrowser.find_element_by_name('CsietParty_Number').send_keys(party)
        oracleBrowser.find_element_by_name('CsietParty_Number_GO').click()
        oracleBrowser.find_element_by_name('Search').click()

        checkIflistNotEmpty = oracleBrowser.find_element_by_css_selector('table[summary="This table lists the item instances based on the selected search."] tr:nth-of-type(2)').text
        if checkIflistNotEmpty != 'No item instances found. Please adjust your search criteria.':
            while True:
                tablePerRow = oracleBrowser.find_elements_by_css_selector('table[summary="This table lists the item instances based on the selected search."] tr')
                for row in range(2,len(tablePerRow)):
                    cellsInRow = oracleBrowser.find_elements_by_css_selector('table[summary="This table lists the item instances based on the selected search."] tr:nth-of-type({}) td'.format(row))
                    while cellsInRow[4].text == '':
                        time.sleep(1)
                        print('waiting for {} row'.format(row-1))
                    ebsInstance = cellsInRow[4].text
                    report[eaNum]['party'][party][ebsInstance] = {}
                    report[eaNum]['party'][party][ebsInstance]['ebs'] = {'description': cellsInRow[1].text, 'sku': cellsInRow[2].text,'type': cellsInRow[3].text, 'status': cellsInRow[9].text, 'quantity': cellsInRow[10].text, 'party_name': cellsInRow[11].text}
                    #go to "Item Instance Details" for each row
                    cellsInRow[14].click()
                    ebsOrder = oracleBrowser.find_element_by_css_selector('input[name="CsietInstance_SalesOrderNum"]')
                    ebsBaseKey = oracleBrowser.find_element_by_xpath('//table[@summary="This table lists the additional attributes."]//tr[contains(.,"VM FLEX Activation Code")]/td[2]')
                    ebsOemKey = oracleBrowser.find_element_by_xpath('//table[@summary="This table lists the additional attributes."]//tr[contains(.,"VM OEM Activation Code")]/td[2]')
        
                    while ebsOrder.get_attribute("value") == '':
                        time.sleep(1)
                    report[eaNum]['party'][party][ebsInstance]['ebs']['orderNum'] = ebsOrder.get_attribute("value")
                    report[eaNum]['party'][party][ebsInstance]['ebs']['baseKey'] = ebsBaseKey.text
                    report[eaNum]['party'][party][ebsInstance]['ebs']['oemKey'] = ebsOemKey.text

                    #contract details
                        #check if not empty
                        #count rows
                        #colect data from cells 
                    oracleBrowser.back()
                    qty_ib += 1
                try:
                    oracleBrowser.find_element_by_link_text('Next').click()
                except:
                    break
print('ORA - 100%')
oracleBrowser.quit()

print('Step 3/4 - EMS - {} IBs'.format(qty_ib))
#login to AP to get party numbers
apBrowser = webdriver.Firefox()
apBrowser.implicitly_wait(30)
apBrowser.get('https://www.vmware.com/admin/')
apBrowser.find_element_by_id('username').send_keys(adUser)
apBrowser.find_element_by_id('password').send_keys(adPasswd)
apBrowser.find_element_by_id('password').submit()
apBrowser.find_element_by_id('username').send_keys(adUser)
apBrowser.find_element_by_id('password').send_keys(adPasswd)
apBrowser.find_element_by_id('password').submit()


i_ib = 0
for eaNum in report:
    for party in report[eaNum]['party']:
        if party != {}:
            for instance in report[eaNum]['party'][party]:
                i_ib += 1
                print('EMS {} - {} - {} - {}%'.format(eaNum,party,instance,round((i_ib-1)*100/qty_ib)))
                apBrowser.find_element_by_link_text('IB Search').click()
                apBrowser.find_element_by_id('ibInstanceNumber').send_keys(instance)
                if report[eaNum]['party'][party][instance]['ebs']['type'][:10] != 'VM License':
                    apBrowser.find_element_by_id('bundle').click()
                apBrowser.find_element_by_id('searchIB').click()
                apBrowser.find_element_by_link_text(instance).click()
                report[eaNum]['party'][party][instance]['ems'] = {}
                order = apBrowser.find_element_by_css_selector('li[class="order_rec"] tr:nth-of-type(2) td:nth-of-type(1)')
                baseKey = apBrowser.find_element_by_css_selector('div[class="ea_detail"] li[class="license_rec"] tr:nth-of-type(2) td:nth-of-type(1)')
                entitlementRowSelenium = apBrowser.find_elements_by_css_selector('table[id="licenseFolderInfo"] tr')
                report[eaNum]['party'][party][instance]['ems']['emsEntitlement'] = []
                for entitlementRow in range(2,len(entitlementRowSelenium)):
                    entitlementId = apBrowser.find_elements_by_css_selector('table[id="licenseFolderInfo"] tr:nth-of-type({}) td'.format(entitlementRow))

                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'] += [{'emsEntitlementId': entitlementId[1].text, 'emsIb': entitlementId[0].text, 'emsProduct': entitlementId[2].text, 'emsLicenseKey': entitlementId[3].text, 'emsFolder': entitlementId[4].text, 'emsKeyQty': entitlementId[5].text, 'emsUpdatableQty': entitlementId[6].text}]
##                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlementRow-2]['emsEntitlementId'] = entitlementId[1].text
##                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlementRow-2]['emsIb'] = entitlementId[0].text
##                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlementRow-2]['emsProduct'] = entitlementId[2].text
##                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlementRow-2]['emsLicenseKey'] = entitlementId[3].text
##                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlementRow-2]['emsFolder'] = entitlementId[4].text
##                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlementRow-2]['emsKeyQty'] = entitlementId[5].text
##                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlementRow-2]['emsUpdatableQty'] = entitlementId[6].text
                report[eaNum]['party'][party][instance]['ems']['orderNum'] = order.text
                if report[eaNum]['party'][party][instance]['ebs']['type'][:10] != 'VM License':
                    report[eaNum]['party'][party][instance]['ems']['baseKey'] = ''
                else:
                    report[eaNum]['party'][party][instance]['ems']['baseKey'] = baseKey.text
                if report[eaNum]['party'][party][instance]['ems']['emsEntitlement'] == []:
                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'] += [{'emsEntitlementId': '', 'emsIb': '', 'emsProduct': '', 'emsLicenseKey': '', 'emsFolder': '', 'emsKeyQty': '', 'emsUpdatableQty': ''}]



                    



print('EMS - 100%')
apBrowser.quit()

print('Step 4/4 - Creating Output')
output = open('report.csv', 'w')
output.write('\
"EA Number",\
"EA Name",\
"EA Segment",\
"EA Type",\
"EA Status",\
"Party Number",\
"EBS Party Name",\
"EBS IB Number",\
"EBS Quantity",\
"EBS Description",\
"EMS Product",\
"EBS SKU",\
"EBS Type",\
"Status",\
"EMS Base Key",\
"EBS Order",\
"EMS Cloud Key",\
"EMS Cloud Qty",\
"EMS Updatable Qty",\
"EMS Folder"\n')
for eaNum in report:
    for party in report[eaNum]['party']:
        if party != {}:
            for instance in report[eaNum]['party'][party]:
                for entitlement in range(len(report[eaNum]['party'][party][instance]['ems']['emsEntitlement'])):
                    if instance == report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlement]['emsIb'] or report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlement]['emsIb'] == '':
                        output.write('"{0}","{1}","{2}","{3}","{4}","{5}","{6}","{7}","{8}","{9}",\
"{10}","{11}","{12}","{13}","{14}","{15}","{16}","{17}","{18}"\n'.format(\
                                    eaNum,\
                                    report[eaNum]['eaName'],\
                                    report[eaNum]['eaSegment'],\
                                    report[eaNum]['eaType'],\
                                    report[eaNum]['eaStatus'],\
                                    party,\
                                    report[eaNum]['party'][party][instance]['ebs']['party_name'],\
                                    instance,\
                                    report[eaNum]['party'][party][instance]['ebs']['quantity'],\
                                    report[eaNum]['party'][party][instance]['ebs']['description'],\
                                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlement]['emsProduct'],\
                                    report[eaNum]['party'][party][instance]['ebs']['sku'],\
                                    report[eaNum]['party'][party][instance]['ebs']['type'],\
                                    report[eaNum]['party'][party][instance]['ebs']['status'],\
                                    report[eaNum]['party'][party][instance]['ems']['baseKey'],\
                                    report[eaNum]['party'][party][instance]['ebs']['orderNum'],\
                                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlement]['emsLicenseKey'],\
                                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlement]['emsKeyQty'],\
                                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlement]['emsUpdatableQty'],\
                                    report[eaNum]['party'][party][instance]['ems']['emsEntitlement'][entitlement]['emsFolder']\
                                    ))
                        
output.close()
print('Output created')