#! python3
from selenium import webdriver



#componnt
def loginToSfdc(driver):
    #login to SFDC requre browser driver
    driver.implicitly_wait(30)
    driver.get('https://login.salesforce.com/')
    driver.find_element_by_id('username').send_keys(sfdcUser)
    driver.find_element_by_id('password').send_keys(sfdcPasswd)
    driver.find_element_by_id('password').submit()

#component
def closeTheBrowser(driver):
    driver.quit()

#component
def openSR(driver, srNum):
    srNum = str(srNum).strip()
    driver.find_element_by_link_text('Cases').click()
    driver.find_element_by_id('phSearchInput').send_keys(srNum)
    driver.find_element_by_id('phSearchInput').submit()
    driver.find_element_by_link_text(srNum).click()

srNum = input('provide SR num:')
srNum = srNum.strip()

browserSFDC = webdriver.Firefox()
loginToSfdc(browserSFDC)
openSR(browserSFDC, srNum)
