from selenium import webdriver


def loginToAP(driver):
    #login to Admin portal requre browser driver    
    driver.implicitly_wait(30)
    #login
    driver.get('https://www.vmware.com/admin/')
    driver.find_element_by_id('username').send_keys(adUser)
    driver.find_element_by_id('password').send_keys(adPasswd)
    driver.find_element_by_id('password').submit()
    driver.find_element_by_id('username').send_keys(adUser)
    driver.find_element_by_id('password').send_keys(adPasswd)
    driver.find_element_by_id('password').submit()

#component
def closeTheBrowser(driver):
    driver.quit()


orders = orders.split()
browserAP = webdriver.Firefox()
loginToAP(browserAP)
browserAP.find_element_by_link_text('Order Search').click()
browserAP.find_element_by_link_text('Legacy Order Search').click()
print('Order;PO;Price;Curency')
for orde in orders:
    browserAP.find_element_by_id('searchValue1').send_keys(orde)
    browserAP.find_element_by_xpath("//form[@id='orderSearch']/table//input[@value='Search']").click()
    po = browserAP.find_element_by_xpath("//tr[@id='row0']/td[4]").text
    price = browserAP.find_element_by_xpath("//tr[@id='row0']/td[6]").text
    price_cur = price[-3:]
    price_val = price[:-4][1:]
    print('%s;%s;%s;%s'  % (orde, po, price_val, price_cur))
    browserAP.find_element_by_xpath("//form[@id='command']/input").click()